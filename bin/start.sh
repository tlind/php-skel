#!/usr/bin/env php
<?php

// must not have any .. elements in it or php-cgi will fail
$_SERVER['APPLICATION_ROOT'] = dirname(__DIR__);

$config = array(
	'var.application-root = "'.$_SERVER['APPLICATION_ROOT'].'/"'
);

$ini = array(
	'auto_prepend_file = '.$_SERVER['APPLICATION_ROOT'].'/http/boot.php'
);

file_put_contents($_SERVER['APPLICATION_ROOT'] . '/etc/app.conf', join("\n", $config));
file_put_contents($_SERVER['APPLICATION_ROOT'] . '/http/.user.ini', join("\n", $ini));

exec("sudo killall lighttpd");
exec("sudo lighttpd -f " . $_SERVER['APPLICATION_ROOT'] . "/etc/default.conf");

?>