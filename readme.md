# Usage

1) Clone the repo

2) Run bin/start.sh

# What you get

1) Portable Web Application Directory

Each time bin/start.sh is run, the server configuration is updated with the hardcoded path of wherever the application directory is,
which means that the application directory can be placed anywhere without you needing to hard code it's path into the server config.

2) PHP reflection of the absolute path of the application root directory.

PHP tells us what the document root is, but does not specify what the application root is, which may be used to contain log files / data files.
By default the application.php file will set the $_SERVER['APPLICATION_ROOT'] path to be the parent of the document root.

4) Auto Prepend

The /boot.php file is auto_prepended to all php execution. The hardcoding of the server path in a php .user.ini file is done for you.
The /boot.php file includes the /application.php, which should be used to define the app.

3) Default dynamic routing

If a file is not found, then the server will automatically rewrite the request to /route.php which provides the dynamic routes or 404 behavior.
Note: route.php will not be run for 'directory' urls (ending with /), which is usually OK (this is a lighttpd rewrite limitation, lighttpd will serve 404 for these urls itself).


5) A server running on whatever hostnames you have pointing to your IP address (without any configuration, localhost).

In future you will be able to set the port and hostname used for each invocation of the application. The PHP app will automatically be able
to reflect on the port and hostname used by looking in $_SERVER['SERVER_PORT'] and $_SERVER['SERVER_NAME'], and the requested host with $_SERVER['HTTP_HOST'].

6) Include path setup

The document root is put on the include path, so put all your files and dependencies in there.
