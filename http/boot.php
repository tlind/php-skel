<?php

// Make up for PHP's lack of specifying what the application root directory is.
$_SERVER['APPLICATION_ROOT'] = dirname($_SERVER['DOCUMENT_ROOT']) . '/';
// put the document root on the include path
set_include_path($_SERVER["DOCUMENT_ROOT"] . PATH_SEPARATOR . get_include_path());

// include the application code
require_once 'application.php';

// the router file must never be included by the auto_prepend file even once,
// because the auto_prepend is run before the server executes the router file,
// which it will do regardless of whether the file has been included already,
// so the route.php file must be separate to the application.php file.

?>